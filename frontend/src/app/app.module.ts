import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from './services/data.service';

import { NgModule} from '@angular/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './core/modules/shared.module';
import {HttpClientModule} from '@angular/common/http';
import {environment} from '@app/env';
import {ROOT_REDUCERS} from '@app/root-store';
import { RouterEffects } from './store/effects/router.effects';
import { IdleTimeoutEffect } from './store/effects/idleTimeout.effects';
import { AuthModule } from './auth/auth.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    StoreDevtoolsModule,
    AuthModule,
    SharedModule,
    StoreModule,
    // StoreRouterConnectingModule.forRoot(),
    InMemoryWebApiModule.forRoot(DataService, { delay: 1500 }),
    StoreModule.forRoot(ROOT_REDUCERS), /* Initialise the Central Store with Application's main reducer*/
    EffectsModule.forRoot([RouterEffects, IdleTimeoutEffect]),
    !environment.production ? StoreDevtoolsModule.instrument({ maxAge: 50, logOnly: false }) : []    
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { 

}
