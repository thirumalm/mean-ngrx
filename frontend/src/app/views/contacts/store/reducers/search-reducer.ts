import { Contact } from '@app/core/models';
import {EntityState, createEntityAdapter} from '@ngrx/entity';
import {createReducer, on} from '@ngrx/store';
import {
  createSuccess,
  loadAllSuccess,
  loadSuccess, removeSuccess,
  updateSuccess
} from '@app/contacts-store/actions/contacts-actions';

type search = 'search';

export const searchFeatureKey: search = 'search';

// This adapter will allow is to manipulate contacts (mostly CRUD operations)
export const contactsAdapter = createEntityAdapter<Contact>({
    selectId: (contact: Contact) => contact.id,
    sortComparer: false
  });

  export interface State extends EntityState<Contact> {
    // additional props here
    ids: string[];
    loading: boolean;
    error: string;
    query: string;
  }
  
  export const INIT_STATE: State = contactsAdapter.getInitialState({
    // additional props default values here
    ids: [],
    loading: false,
    error: null,
    query: null
  });

  export const reducer = createReducer<State>(
    INIT_STATE,
    on(loadAllSuccess, (state, {contacts}) =>
    contactsAdapter.addAll(contacts, state)
    ),
  );