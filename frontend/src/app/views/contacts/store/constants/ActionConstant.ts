
export class ActionConstant{
    public static LOAD_ALL: string = '[Contacts] Load all';
    public static LOAD: string = '[Contacts] Load';
    public static SEARCH: string = '[Contacts] Search';
}