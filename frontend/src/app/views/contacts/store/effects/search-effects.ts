import { Injectable } from '@angular/core';
import { map, pluck, switchMap } from 'rxjs/operators';
import {Actions, createEffect, ofType} from '@ngrx/effects';

import { search, loadAllSuccess } from '@app/contacts-store/actions/contacts-actions';
import {ContactsService} from '../../services/contacts.service';

@Injectable()
export class SearchEffects{
    constructor(private actions$: Actions,
        private contactsService: ContactsService){}

    search$ = createEffect(()=> this.actions$.pipe(
        ofType(search),
        pluck('searchText'),
        switchMap((searchText: string) => this.contactsService.doSearch(searchText).pipe(
            map(contacts => {
                console.log('contacts : ',contacts);
                return loadAllSuccess({contacts})
            })
        ))
    ));
}