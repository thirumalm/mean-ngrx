import * as fromContacts from './reducers/contacts-reducer';
import * as fromSearch from './reducers/search-reducer';
import {Action, combineReducers, createFeatureSelector, createSelector} from '@ngrx/store';

export interface MyFeatureState {
  [fromContacts.contactFeatureKey]: fromContacts.State;
  [fromSearch.searchFeatureKey]: fromSearch.State;
};

/** Provide reducers with AoT-compilation compliance */
export function reducers(state: MyFeatureState | undefined, action: Action) {
  return combineReducers({
    [fromContacts.contactFeatureKey]: fromContacts.reducer,
    [fromSearch.searchFeatureKey]: fromSearch.reducer
  })(state, action)
}


/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
 */

export const getContactsState = createFeatureSelector<MyFeatureState>(fromContacts.contactFeatureKey);

export const getContactsEntitiesState = createSelector(
  getContactsState,
  state => state.contacts
);

export const {
  selectAll: getAllContacts,
} = fromContacts.contactsAdapter.getSelectors(getContactsEntitiesState);

export const getContactById = (id: number) => createSelector(
  getContactsEntitiesState,
  fromContacts.getContactById(id)
);
