import { createAction, props } from '@ngrx/store';
import { Contact } from '@app/core/models';
import { ActionConstant } from '../constants/Actionconstant';

export const loadAll = createAction(
    ActionConstant.LOAD_ALL
);

export const load = createAction(
    ActionConstant.LOAD,
    props<{id:number}>()
);

export const create = createAction(
    '[Contacts] Create',
    props<{contact: Contact}>()
  );
  
  export const update = createAction(
    '[Contacts] Update',
    props<{contact: Partial<Contact>}>()
  );
  
  export const remove = createAction(
    '[Contacts] Remove',
    props<{id: number}>()
  );
  
  export const loadAllSuccess = createAction(
    '[Contacts] Load all success',
    props<{contacts: Contact[]}>()
  );
  
  export const loadSuccess = createAction(
    '[Contacts] Load success',
    props<{contact: Contact}>()
  );
  
  export const testSuccess = createAction(
    '[Contacts] Test success'
  );

  export const createSuccess = createAction(
    '[Contacts] Create success',
    props<{contact: Contact}>()
  );
  
  export const updateSuccess = createAction(
    '[Contacts] Update success',
    props<{contact: Partial<Contact>}>()
  );
    
  export const removeSuccess = createAction(
    '[Contacts] Remove success',
    props<{id: number}>()
  );
    
  export const failure = createAction(
    '[Contacts] Failure',
    props<{err: {concern: 'CREATE' | 'PATCH', error: any}}>()
  );
  
  export const search = createAction(
    ActionConstant.SEARCH,
    props<{searchText: string}>()
  )