import { Component, OnInit } from '@angular/core';
import { Contact } from '@app/core/models';
import { ContactsStoreFacade } from '@app/contacts-store/facades/contacts.store-facade';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-index',
  templateUrl: './contact-index.component.html',
  styleUrls: ['./contact-index.component.scss']
})
export class ContactIndexComponent implements OnInit {  
  public contacts: Contact[];
  public contacts$ = this.contactsFacade.contacts$;  
  
  constructor(private contactsFacade:ContactsStoreFacade, private router:Router) {    
  }

  ngOnInit(): void {    
  }

  public doSearch(searchText: string): void{
    this.contactsFacade.searchContacts(searchText);
  }

  editContact(contact: Contact) {
    this.router.navigate(['/contacts', contact.id, 'edit']);
  }

  showContact(contact: Contact) {
    this.router.navigate(['/contacts', contact.id]);
  }

  deleteContact(contact: Contact) {
    const r = confirm('Are you sure?');
    if (r) {
      this.contactsFacade.deleteContact(contact.id);
    }
  }
}
