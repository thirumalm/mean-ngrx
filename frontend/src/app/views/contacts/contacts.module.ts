import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import { ContactDetailsComponent } from './contact-details/contact-details.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactNewComponent } from './contact-new/contact-new.component';
import { ContactIndexComponent } from './contact-index/contact-index.component';
import { ContactsRoutingModule} from './contacts-routing.module';
import { SharedModule } from '../../core/modules/shared.module';
import { ContactsComponent } from './contacts.component';
import { ContactsService } from '../contacts/services/contacts.service';
import {reducers} from '@app/contacts-store';
import {ContactsEffects} from './store/effects/contacts-effects';
import {ContactsStoreFacade} from '@app/contacts-store/facades/contacts.store-facade';
import { SearchEffects } from './store/effects/search-effects';

@NgModule({
  declarations: [
    ContactsComponent,
    ContactDetailsComponent,
    ContactEditComponent,
    ContactNewComponent,
    ContactIndexComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ContactsRoutingModule,
    StoreModule.forFeature('contacts', reducers),
    EffectsModule.forFeature([ContactsEffects, SearchEffects])
  ],
  providers:[ContactsService, ContactsStoreFacade]
})
export class ContactsModule { }
