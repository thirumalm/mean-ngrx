import { Component, OnInit } from '@angular/core';
import { Contact } from '@app/core/models';
import { Router} from '@angular/router';
import { ContactsStoreFacade} from '@app/contacts-store/facades/contacts.store-facade';
@Component({
  selector: 'app-contact-new',
  templateUrl: './contact-new.component.html',
  styleUrls: ['./contact-new.component.scss']
})
export class ContactNewComponent implements OnInit {

  constructor(private router:Router, private contactsFacade: ContactsStoreFacade) { }

  ngOnInit(): void {
  }

  submitted(contact: Contact) {
    this.contactsFacade.createContact(contact);
    this.router.navigate(['/contacts']);


  }


}
