import { Injectable } from '@angular/core';
import {Observable, pipe, of} from 'rxjs';
import { map, filter } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Contact} from '@app/core/models';
import {environment} from '@app/env';
// import { ContactsModule } from '../contacts.module';

/*@Injectable({
  providedIn: 'root'
})*/
@Injectable()
export class ContactsService {

  constructor(private http: HttpClient ) { }

  index(): Observable<Contact[]> {
    return this.http.get<Contact[]>(`${environment.appApi.baseUrl}/contacts`);
  }

  show(conactId: number): Observable<Contact> {
    return this.http.get<Contact>(`${environment.appApi.baseUrl}/contacts/${conactId}`);
  }
  create(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(`${environment.appApi.baseUrl}/contacts`, contact);
  }

  update(contact: Partial<Contact>): Observable<Contact> {    
    return this.http.post<Contact>(`${environment.appApi.baseUrl}/contacts/${contact.id}`, contact);
  }
  
  doSearch(searchText: string): Observable<Contact[]> {
    let allContacts:Contact[];
    console.log("doSearch function from contacts.services");
    //return this.index().pipe(map( (contacts: Contact[]) => contacts.filter((contact: Contact) => contacts.name) ))}
    return of([]);
  };
  destroy(id: number): Observable<Contact> {
    return this.http.delete<Contact>(`${environment.appApi.baseUrl}/contacts/${id}`);
  }

}
