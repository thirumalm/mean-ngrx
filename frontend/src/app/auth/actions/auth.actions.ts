import { props, createAction } from '@ngrx/store';
import { User } from '../models';
import { Credentials } from '../models';

export const login = createAction(
  '[Login Page] Login',
  props<{ credentials: Credentials }>()
);


export const loginSuccess = createAction(
  '[Auth/API] Login Success',
  props<{ user: User }>()
);

export const loginFailure = createAction(
  '[Auth/API] Login Failure',
  props<{ error: any }>()
);

export const loginRedirect = createAction('[Auth/API] Login Redirect');


/* ------------------------ */

export const logout = createAction('[Auth] Logout');
export const logoutConfirmation = createAction('[Auth] Logout Confirmation');
export const logoutConfirmationDismiss = createAction(
  '[Auth] Logout Confirmation Dismiss'
);
