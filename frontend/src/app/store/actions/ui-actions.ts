import { createAction, props, ActionCreator } from '@ngrx/store';
/**
 * CreateAction has two parameters which is (ActionType as string (Mandatory), Props as function with object params)
*/
export const setCurrentTitle = createAction(
    '[UI] Set current title',
     props<{payload: string}>()
);

export const idleTimeout = createAction(
    '[UI] Idel Timeout'
)