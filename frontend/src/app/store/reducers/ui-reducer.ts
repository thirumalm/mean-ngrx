import {createReducer, on, ActionReducer, Action} from '@ngrx/store';
import {setCurrentTitle} from '@app/root-store/actions/ui-actions';
import {setState} from '@app/core/helpers/ngrx.helpers';

export interface State {
  currentTitle: string;
  
}

export const INIT_UI_STATE: State = {
  currentTitle: undefined
};

//export const reducer: ActionReducer<UiState, Action>
/**
 * createReducer has two parameters which are (InitialState as any datatype,  ActionListener)
*/
export const reducer = createReducer(
  INIT_UI_STATE,
  on(setCurrentTitle, (state, {payload: currentTitle}) =>{
      console.log(setState({currentTitle}, state),{currentTitle},state," : from createReducer method in ui-reducer.ts : ");
    return setState({currentTitle}, state);
  })
);

// SELECTORS
export const getCurrentTitle = (state: State) => state ? state.currentTitle : null;
