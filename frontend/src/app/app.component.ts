import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import * as fromRoot from '@app/root-store';
import {select, Store} from '@ngrx/store';
// import { PolicyService } from './services/policy.service';

@Component({
  selector: 'app-root',
  templateUrl:'app.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

  currentPageTitle$ = this.store.pipe(select(fromRoot.getCurrentTitle));
  
  constructor(private store: Store<fromRoot.State>) {}

  ngOnInit() {
    
  }
  
}
