import { Injectable } from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api'
@Injectable()
export class DataService implements InMemoryDbService{

  constructor() { }
  createDb(){

    let  contacts =  [
      {  id:  1,  name: 'Vigneshwaran', email:'vickybees@gmail.com', phone:'123456' },
      {  id:  2,  name: 'Thirumaal', email:'info.thirumal@gmail.com', phone:'123456' }
     ];
     
   return {contacts};
  }
}