import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Fix = new Schema({
    title: {
        type: String
    },
    responsible: {
        type: String
    },
    description: {
        type: String
    },
    severity: {
        type: String
    },
    status: {
        type: String,
        default: 'Fixed'
    }
});

export default mongoose.model('Fix', Fix);