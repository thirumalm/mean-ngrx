import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Contact = new Schema({
    name: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: String
    }
});

export default mongoose.model('Contact', Contact);