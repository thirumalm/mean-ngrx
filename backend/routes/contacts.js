import express from 'express';
import Contact from '../models/Contact';

const router = express.Router();

router.route('/contacts').get((req, res) => {
    Contact.find((err, contacts) => {
        if (err){
            console.log(err);
        } else {
            res.json(contacts);
        }            
    });
});

router.route('/contacts/:id').get((req, res) => {
    Contact.findById(req.params.id, (err, contact) => {
        if (err){
            console.log(err);
        }else{
            res.json(contact);
        }            
    });
});

//router.route('/contacts/add').post((req, res) => {
router.route('/contacts').post((req, res) => {
    let contact = new Contact(req.body);
    contact.save()
        .then(contact => {
            //res.status(200).json({'contact': 'Added successfully'});
            res.json(contact);
        })
        .catch(err => {
            res.status(400).send('Failed to create new record');
        });
});

//router.route('/contacts/update/:id').post((req, res) => {
router.route('/contacts/:id').post((req, res) => {
    Contact.findById(req.params.id, (err, contact) => {
        if (!contact)
            return next(new Error('Could not load document'));
        else {
            contact.name = req.body.name;
            contact.email = req.body.email;
            contact.phone = req.body.phone;

            contact.save().then(contact => {
                //res.json('Update done');
                res.json(contact);
            }).catch(err => {
                res.status(400).send('Update failed');
            });
        }
    });
});

//router.route('/contacts/delete/:id').get((req, res) => {
router.route('/contacts/:id').get((req, res) => {
    Contact.findByIdAndRemove({id: req.params.id}, (err, contact) => {
        if (err){
            res.json(err);
        }else{
            res.json(contacts.id);
            //res.json('Remove successfully');
        }            
    });
});

module.exports = router;