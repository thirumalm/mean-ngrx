import express from 'express';
import Fix from '../models/Fix';

const router = express.Router();

router.route('/fixes').get((req, res) => {
    Fix.find((err, fix) => {
        if (err)
            console.log(err);
        else
            res.json(fix);
    });
});

router.route('/fixes/:id').get((req, res) => {
    Fix.findById(req.params.id, (err, fix) => {
        if (err)
            console.log(err);
        else
            res.json(fix);
    });
});

router.route('/fixes/add').post((req, res) => {
    let fix = new Fix(req.body);
    fix.save()
        .then(fix => {
            res.status(200).json({'fix': 'Added successfully'});
        })
        .catch(err => {
            res.status(400).send('Failed to create new record');
        });
});

router.route('/fixes/update/:id').post((req, res) => {
    Fix.findById(req.params.id, (err, fix) => {
        if (!fix)
            return next(new Error('Could not load document'));
        else {
            fix.title = req.body.title;
            fix.responsible = req.body.responsible;
            fix.description = req.body.description;
            fix.severity = req.body.severity;
            fix.status = req.body.status;

            fix.save().then(fix => {
                res.json('Update done');
            }).catch(err => {
                res.status(400).send('Update failed');
            });
        }
    });
});

router.route('/fixes/delete/:id').get((req, res) => {
    Fix.findByIdAndRemove({id: req.params.id}, (err, fix) => {
        if (err)
            res.json(err);
        else
            res.json('Remove successfully');
    });
});

module.exports = router;