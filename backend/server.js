import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import path from 'path';

import contacts from './routes/contacts';
import fixes from './routes/fixes';

const app = express();

app.use(cors());
app.use(bodyParser.json());
//app.use(express.static(path.join(__dirname, 'dist/frontend')));
app.use('/', contacts);
app.use('/', fixes);

mongoose.connect('mongodb://localhost:27017/Contacts');

const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB database connection established successfully!');
});

app.listen(4000, () => console.log('Express server running on port 4000'));